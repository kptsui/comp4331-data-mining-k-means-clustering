package main;

import java.util.ArrayList;

public class Cluster {
	public String id;
	public ArrayList<Point> points;
	public Point centroid;
	
	public Cluster(String id, Point centroid) {
		this.id = id;
		this.points = new ArrayList<Point>();
		this.centroid = centroid;
	}
	
	@Override
	public String toString(){
		StringBuilder stringBuilder = new StringBuilder("cluster " + id + ": ");
		
		// debug purpose
		//stringBuilder.append("Centroid: (" + centroid.x + ", " + centroid.y + "), ");
		
		if(points != null && !points.isEmpty()) {
			stringBuilder.append(points.get(0).label);
			for(int i = 1; i < points.size(); ++i)
				stringBuilder.append(", ").append(points.get(i).label);
		}
		return stringBuilder.toString();
	}
}
