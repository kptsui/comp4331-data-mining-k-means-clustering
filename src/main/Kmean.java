package main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class Kmean {

	public final static int RUNS = 20;
	ArrayList<Point> points;
	ArrayList<Cluster> clusters;
	int input_k;

	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.print("Enter the number of clusters: ");
		int num_cluster = reader.nextInt();
		reader.close();

		try {
			ArrayList<Point> points = readInputFile();

			// make sure data points is more than input K cluster
			if (num_cluster > points.size()) {
				System.err.println("Error: The input number of clusters is more than the data!!");
				return;
			}

			double SSE[] = new double[RUNS];
			Kmean kmeans[] = new Kmean[RUNS];
			double min_SSE = Double.MAX_VALUE;
			Kmean solution = null;

			for (int i = 0; i < RUNS; ++i) {
				Kmean kmean = new Kmean(deepCopy(points), num_cluster);
				kmean.learningStep();

				kmeans[i] = kmean;
				SSE[i] = kmean.SEE();

				if (SSE[i] < min_SSE) {
					min_SSE = SSE[i];
					solution = kmeans[i];
				}
				// debug
				//System.out.println("SSE: " + SSE[i] + "\nCluster: " + kmeans[i].clusters);
			}
			// debug
			//System.out.println("Min SSE: " + min_SSE + "\nSolution: " + solution.clusters);

			for (Cluster cluster : solution.clusters) {
				System.out.println(cluster);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// Initializes clusters, pick random point from data as initial centroid
	public Kmean(ArrayList<Point> points, int num_clusters) throws IOException {
		this.points = points;

		this.input_k = num_clusters;

		this.clusters = initClusterCentroid();
	}

	// read data to ArrayList<Point> points
	public static ArrayList<Point> readInputFile() throws IOException {
		ArrayList<Point> points = new ArrayList<Point>();
		BufferedReader in = new BufferedReader(new FileReader("input.txt"));
		// ignore first line
		String line = in.readLine();
		String[] splits;
		while ((line = in.readLine()) != null) {
			line = line.replace("|", "").trim();
			splits = line.split(" ");
			double x = Double.parseDouble(splits[1]);
			double y = Double.parseDouble(splits[2]);
			points.add(new Point(splits[0], x, y));
		}
		in.close();
		return points;
	}

	public void learningStep() {
		boolean finish = false;

		// debug purpose
		/*for (Cluster cluster : clusters) {
			System.out.println(cluster);
		}
		System.out.println("-------------------------------------------------");*/

		while (!finish) {
			// Clear cluster state
			for (Cluster cluster : clusters) {
				cluster.points.clear();
			}

			ArrayList<Point> lastCentroids = getCentroids();

			// Assign points to the closer cluster (shortest distance)
			// For each point, calculate all the distance from the point to all
			// cluster centroid
			// pick the cluster with shortest distance
			assignCluster();

			// update Clusters' centroid.
			updateCentroids();

			ArrayList<Point> currentCentroids = getCentroids();

			// Calculates total distance between new and old Centroids
			double distance = 0;
			for (int i = 0; i < lastCentroids.size(); ++i) {
				distance += Point.distance(lastCentroids.get(i), currentCentroids.get(i));
			}

			if (distance == 0) {
				finish = true;
			}

			// debug purpose
			/*for (Cluster cluster : clusters) {
				System.out.println(cluster);
			}
			System.out.println("-------------------------------------------------");*/
		}
	}

	public static ArrayList<Point> deepCopy(ArrayList<Point> points){
		ArrayList<Point> copy = new ArrayList<Point>();
		for(Point p : points)
			copy.add(new Point(p.label, p.x, p.y));
		return copy;
	}
	
	private ArrayList<Point> getCentroids() { // deep copy
		ArrayList<Point> centroids = new ArrayList<Point>(input_k);
		for (Cluster cluster : clusters) {
			centroids.add(new Point("", cluster.centroid.x, cluster.centroid.y));
		}
		return centroids;
	}

	private void assignCluster() {
		double max = Double.MAX_VALUE;
		double min = max;
		int cluster_pos = 0;
		double distance = 0.0;

		for (Point point : points) {
			min = max;
			for (int i = 0; i < clusters.size(); i++) {
				distance = Point.distance(point, clusters.get(i).centroid);
				if (distance < min) {
					min = distance;
					cluster_pos = i;
				}
			}
			Cluster assign_cluster = clusters.get(cluster_pos);
			assign_cluster.points.add(point);

			point.setCluster(assign_cluster);
		}
	}

	private void updateCentroids() {
		for (Cluster cluster : clusters) {
			int num_points = cluster.points.size();
			if (num_points > 0) {
				double sumX = 0;
				double sumY = 0;

				for (Point point : cluster.points) {
					sumX += point.x;
					sumY += point.y;
				}

				cluster.centroid.x = sumX / num_points;
				cluster.centroid.y = sumY / num_points;
				
				cluster.centroid.x = Double.parseDouble(String.format("%.4f", cluster.centroid.x));
				cluster.centroid.y = Double.parseDouble(String.format("%.4f", cluster.centroid.y));
			}
		}
	}

	private double SEE() { // Sum Square Error for single run
		double SEE = 0;
		for (Cluster c : clusters) {
			for (Point p : c.points) {
				SEE += Math.pow(Point.distance(p, c.centroid), 2);
			}
		}
		return SEE;
	}

	private ArrayList<Cluster> initClusterCentroid() { // k-means++ algorithm
		Random ran = new Random();
		int random_num = ran.nextInt(input_k); // 0 to k - 1

		ArrayList<Cluster> clusters = new ArrayList<>();
		clusters.add(new Cluster("1", points.get(random_num)));

		for (int i = 1; i < input_k; ++i) {
			
			// assign points to temp clusters
			for (Point p : points) {
				double min = Double.MAX_VALUE;
				Cluster tempCluster = null;
				// find cluster closest to the Point
				for (Cluster c : clusters) {
					double distance = Point.distance(p, c.centroid);
					if (distance < min) {
						min = distance;
						tempCluster = c;
					}
				}
				// closest cluster c is found
				// assign the point to closest cluster
				tempCluster.points.add(p);
			}

			// find longest point as centroid
			double max = Double.MIN_VALUE;
			Point centroid = null;
			for (Cluster c : clusters) {
				for (Point p : c.points) {
					double distance = Point.distance(p, c.centroid);
					if (distance > max) {
						max = distance;
						centroid = p;
					}
				}
			}

			clusters.add(new Cluster(String.valueOf(i + 1), centroid));
			
			// clear points in clusters
			for(Cluster c : clusters){
				c.points.clear();
			}
		}
		
		return clusters;
	}
	
	private void initRandomClusterCentroid(){ // Random centroid method (not accurate!!!!)
		this.clusters = new ArrayList<>();

		ArrayList<Integer> random_nums = new ArrayList<Integer>();
		for (int i = 0; i < points.size(); ++i) {
			random_nums.add(new Integer(i));
		}

		Collections.shuffle(random_nums);
		if (points.size() > input_k) {
			for (int i = input_k; i < random_nums.size(); ++i) {
				random_nums.remove(i);
			}
		}
		Collections.sort(random_nums);

		for (int i = 0; i < input_k; ++i) {
			Point centroid = points.get(random_nums.get(i));
			clusters.add(new Cluster(String.valueOf(i + 1), centroid));
		}
	}
}
