package main;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;

public class Point {
	public String label;
	public double x;
	public double y;
	public String cluster_id;
	
	public Point(String label, double x, double y){
		this.label = label;
		this.x = x;
		this.y = y;
	}
	
	public static double distance(Point p, Point centroid){
		return Math.sqrt( Math.pow(p.y - centroid.y, 2) + Math.pow(p.x - centroid.x, 2) );
	}
	
	public static Point createRandomPoint(int min, int max) {
    	Random r = new Random();
    	double x = min + (max - min) * r.nextDouble();
    	double y = min + (max - min) * r.nextDouble();
    	return new Point("", x, y);
    }
	
	public static ArrayList<Point> createRandomPoints(int min, int max, int number) {
		ArrayList<Point> points = new ArrayList<Point>(number);
    	for(int i = 0; i < number; i++) {
    		points.add(createRandomPoint(min, max));
    	}
    	return points;
    }
	
	public void setCluster(Cluster cluster) {
        this.cluster_id = cluster.id;
    }
	
	@Override
	public String toString(){
		return label + ": " + x + ", " + y;
	}
}
